# REST samples SAP ES5

Sample Postman REST calls against SAP Gateway ES5 demo system.

This repo contains a Postman collection with sample requests against the public SAP Gateway system ES5.

To perform the requests, you need to have a valid user for the ES5 system. See this blog on how to get access to the system: https://blogs.sap.com/2017/12/05/new-sap-gateway-demo-system-available/

Before you run the samples, change the username and password to your credentials:

1. Select collection and right click
2. Choose Edit
3. In the tab Pre-request scripts, insert your username and password

After importing the collection, it is available under the name SAP ES5 Gateway. Main configuration like UIDPW is configured at the collection level. Each service added is available in a sub folder.

# Services

Currently, the following services are added to the Postman collection:

* SEPMRA_OVW